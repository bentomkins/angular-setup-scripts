# angular setup scripts


[Resetting Node](#Reset)&nbsp;&nbsp;|&nbsp;&nbsp;[Installing Node](#Installing)
<br><br>
[Setting-up Angular](#SettingUp)&nbsp;&nbsp;|&nbsp;&nbsp;[Setting-up an Angular workspace](#Workspaces)&nbsp;&nbsp;|&nbsp;&nbsp;[FAQ](#FAQ)&nbsp;&nbsp;|&nbsp;&nbsp;[Random](#Random)
<br><br>

#### <a name="Reset"></a>Uninstalling Node (windows) :
1. `npm cache clean --force`
2. Uninstall from Programs & Features with the uninstaller
3. Reboot
4. Look for these folders and remove them (and their contents) if any still exist. Depending on the version you installed, UAC settings, and CPU architecture, these may or may not exist:
```
C:\Program Files (x86)\Nodejs
C:\Program Files\Nodejs
C:\Users\{User}\AppData\Roaming\npm (or %appdata%\npm)
C:\Users\{User}\AppData\Roaming\npm-cache (or %appdata%\npm-cache)
C:\Users\{User}\.npmrc (and possibly check for that without the . prefix too)
C:\Users\{User}\AppData\Local\Temp\npm-*
```
5. Check your `%PATH%` environment variable to ensure no references to Nodejs or npm exist.
6. If it's still not uninstalled, type where node at the command prompt and you'll see where it resides -- delete that (and probably the parent directory) too.
7. Reboot, for good measure.

#### <a name="Installing"></a>Installing Node :
// Find the specific version of Node
https://nodejs.org/dist/


#### <a name="SettingUp"></a>Installing Angular globally:
npm install -g @angular/cli@7.3.7


#### <a name="Workspace"></a>Setting-up an Angular workspace :
ng new ui-library --create-application=false --routing=true --style=scss


#### <a name="Library"></a>Adding an Angular library to a workspace :
1. cd ui-library
2. npm install
3. ng generate @schematics/angular:library ui-library --prefix=hsbc


#### <a name="Component"></a>Adding a component to a library :

1. ng generate @schematics/angular:component button --project=ui-library --module=ui-library.module.ts --export --style=scss
2. replace the contents of projects/ui-library/src/lib/button/button.component.html with:
```
<button (click)="onclick()">{{buttonText}}</button>
```
3. replace the contents of projects/ui-library/src/lib/button/button.component.ts with:
```javascript
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'hsbc-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  @Input() buttonText: string;
  @Output() click: EventEmitter<void> = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  onclick() {
    this.click.emit();
  }
}

```

#### <a name="StoryBook"></a>Adding StoryBook to a workspace :
1. npx -p @storybook/cli sb init --type angular
2. cd .storybook
3. Replace the “extends” line with the line below, in the tsconfig.json:
```javascript
"extends": "../projects/ui-library/tsconfig.lib.json",
```
4. create “webpack.config.js” file to the .storybook directory, with the following contents:
```javascript
const path = require('path');
const includePath = path.resolve(__dirname, '..')
module.exports = {
    module: {
        rules: [
            {
                test: [/\.stories\.tsx?$/, /index\.ts$/],
                loaders: [],
                include: [path.resolve(__dirname, '../src')],
                enforce: 'pre'
            },
            {
                test: /\.css$/,
                include: includePath,
                use: ['to-string-loader', 'css-loader']
            }
        ]
    }
};
```
5. Install the loaders:
```
npm install to-string-loader --save-dev
npm install css-loader --save-dev
```
6. Install the storybook plugins:
  - `npm install --save-dev @storybook/addon-a11y @storybook/addon-knobs @storybook/addon-viewport`
  - in the .storybook/addons.js file, replace `import '@storybook/addon-notes/register';`
  with the following:
  ```
  import '@storybook/addon-notes/register-panel';
  import '@storybook/addon-a11y/register';
  import '@storybook/addon-knobs/register';
  import '@storybook/addon-viewport/register';
  ``` 



#### <a name="Wire-up"></a>Adding a library component to the StoryBook :
1. replace the contents of src/stories/index.stories.ts with:
```javascript
import { storiesOf, moduleMetadata, addDecorator, addParameters } from '@storybook/angular';
import { action } from '@storybook/addon-actions';
import { withA11y } from '@storybook/addon-a11y';
import { withNotes } from '@storybook/addon-notes';
import { withKnobs, text, number, boolean, array, select, radios, color, date, button } from '@storybook/addon-knobs';

import { ButtonComponent } from '../../projects/ui-library/src/lib/button/button.component';
import { TextboxComponent } from '../../projects/ui-library/src/lib/textbox/textbox.component';


addDecorator(withKnobs);
addDecorator(withA11y);
addDecorator(withNotes);

const ui = storiesOf('ui components', module);


ui.add('hsbc-button', () => ({
  component: ButtonComponent,
  props: {
    buttonText: text('Button text', 'hi hsbc ppl 👋'),
    click: action('You clicked me...')
  },
}),
{
  notes: 'Insert notes and relevant information here',
});

```

## Code Quality:
#### <a name="husky"></a>Adding opinionated codestyle with Husky and Prettier :
1. `npm install husky prettier lint-staged --save-dev`
2. add the following after devDependencies in the package.json

```json
"husky": {
  "hooks": {
    "pre-commit": "lint-staged"
  }
},
"lint-staged": {
  "projects/**/*.ts": [
    "prettier --write",
    "git add"
  ]
}
```

3. create a file in the root and name it .prettierrc, with the following contents:
```
{
  "printWidth": 120,
  "singleQuote": true,
  "tabWidth": 2
}
```
4. add the following scripts to the package.json:
```
"format:check": "prettier --list-different 'projects/**/*.ts'",
"format:write": "prettier --write 'projects/**/*.ts'",
```

## Code coverage
1. add the following script to the package.json
```
"test:lib-coverage": "ng test ui-library --code-coverage --watch=false",
```


#### <a name="build"></a>Building and publishing a library :

1. Install dependencies
```
npm install copyfiles --save-dev
npm install npm-run-all --save-dev
```
2. add the following scripts to the package.json
```
"build:lib": "run-s build:ui pack:ui",
"build:ui": "npm run copy:readme && ng build ui-library",
"pack:ui": "cd dist/ui && npm pack",
"copy:readme": "copyfiles ./README.md ./dist/ui",
```

#### <a name="commits"></a>Adding conventional commits :
https://commitlint.js.org/
https://www.conventionalcommits.org/
```
npm install @commitlint/cli @commitlint/config-conventional @commitlint/prompt --save-dev
```
3. add the following after lint-staged in the package.json
```json
"commitlint": {
  "extends": [
    "@commitlint/config-conventional"
  ]
}
```
4. add the following hook after pre-commit
```
"commit-msg": "commitlint -E HUSKY_GIT_PARAMS"
```

#### <a name="commits"></a>e2e tests :
https://www.cypress.io/
```
npm install cypress --save-dev
```
```json
{
    "integrationFolder": "cypress/integration/examples",
    "fixturesFolder": "cypress/fixtures",
    "reporter": "mochawesome",
    "reporterOptions": {
      "reportDir": "cypress/report/mochawesome-report",
      "overwrite": false,
      "html": false,
      "json": true,
      "timestamp": "mmddyyyy_HHMMss"
    },
    "video": true,
    "pluginsFile": "cypress/plugins/index.js",
    "supportFile": "cypress/plugins/index.js",
    "screenshotsFolder": "cypress/report/mochawesome-report/assets",
    "videosFolder": "cypress/videos",
    "env": {
      "foo": "bar",
      "some": "value"
    },
    "chromeWebSecurity": true,
    "trashAssetsBeforeRuns": true
}

```

#### <a name="commits"></a>Configure Karma for headless chrome single run :

1. Replace the contents of the `projects/ui-library/karma.conf.js` file with the following :

```javascript
// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-coverage-istanbul-reporter'),
      require('@angular-devkit/build-angular/plugins/karma')
    ],
    client: {
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    coverageIstanbulReporter: {
      dir: require('path').join(__dirname, '../../coverage/ui'),
      reports: ['html', 'lcovonly'],
      fixWebpackSourcePaths: true
    },
    reporters: ['progress', 'kjhtml'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: false,
    browsers: ['ChromeHeadless'],
    singleRun: true,
    restartOnFileChange: true
  });
};
```
2. ng test

#### <a name="commits"></a>pipeline webserver :


#### <a name="commits"></a>test runner :
```
npm install mocha@5.2.0 mochawesome mochawesome-merge --save-dev
npm install yargs ls rimraf --save-dev
```
cypress_runner.js
```javascript
const cypress = require('cypress')
const yargs = require('yargs')
const { merge } = require('mochawesome-merge')
const marge = require('mochawesome-report-generator')
const rm = require('rimraf')
const cypressConfig = require('./cypress')
const ls = require('ls')
const argv = yargs.options({
    'browser': {
        alias: 'b',
        describe: 'choose browser that you wanna run tests on',
        default: 'chrome',
        choices: ['chrome', 'electron']
    },
    'spec': {
        alias: 's',
        describe: 'run test with specific spec file',
        default: 'cypress/integration/examples/*.spec.js'
    }
}).help()
  .argv

const reportDir = cypressConfig.reporterOptions.reportDir
const reportFiles = `${reportDir}/*.json`
// list all of existing report files
ls(reportFiles, { recurse: true }, file => console.log(`removing ${file.full}`))

// delete all existing report files
rm(reportFiles, (error) => {
    if (error) {
        console.error(`Error while removing existing report files: ${error}`)
        process.exit(1)
    }
    console.log('Removing all existing report files successfully!')
})

cypress.run({
    browser: argv.browser,
    spec: argv.spec
}).then((results) => {
    const reporterOptions = {
        reportDir: results.config.reporterOptions.reportDir,
    }
    generateReport(reporterOptions)
}).catch((error) => {
    console.error('errors: ', error)
    process.exit(1)
})

function generateReport(options) {
    return merge(options).then((report) => {
        marge.create(report, options)
    })
}
```

cypress/support/index.js
```javascript
import './commands'
import '@cypress/code-coverage/support'
const addContext = require('mochawesome/addContext')

// return filePath to addContext to mochawesome reporter
Cypress.on('test:after:run', (test, runnable) => {
    if (test.state === 'failed') {
        const screenshotFileName = `${runnable.parent.title} -- ${test.title} (failed).png`
        addContext({ test }, `assets/${Cypress.spec.name}/${screenshotFileName}`)
    }
})
```

2. add script to package.json
```
"test:runner": "node cypress_runner -b chrome",
```
#### <a name="commits"></a>generate live artifacts :
